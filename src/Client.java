/*
 2. Stwórzmy klasę Client. Każdy klient posiada typ sprawy który musi podać oraz swój pesel, dlatego:
        - stwórz konstruktor w klasie klient, która jako parametr przyjmuje typ sprawy oraz pesel i ustawia wartości tych pól w klasie.
        - stwórz gettery oraz settery tych pól.
 */
public class Client {
    private TypSprawy sprawa;
    private String pesel;

    public Client(TypSprawy sprawa, String pesel) {
        this.sprawa = sprawa;
        this.pesel = pesel;
    }

    public TypSprawy getSprawa() {
        return sprawa;
    }

    public void setSprawa(TypSprawy sprawa) {
        this.sprawa = sprawa;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    @Override
    public String toString() {
        return "Client{" +
                "sprawa=" + sprawa +
                ", pesel='" + pesel + '\'' +
                '}';
    }
}