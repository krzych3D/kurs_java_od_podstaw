import java.util.InputMismatchException;
import java.util.Scanner;

/*
Zadanie 35:
    TO ZADANIE WYDAJE SIĘ BYĆ TRUDNE, ALE NIE JEST. CHODZI W NIM WYŁĄCZNIE O PODKREŚLENIE RELACJI MIĘDZY OBIEKTAMI.
    Stwórz system obsługi. Zakładamy istnienie urzędu w którym znajdują się pokoje, w których można załatwić naszą sprawę.
     Klient powinien mieć do dyspozycji linię poleceń, która przyjmuje ich rejestracje.
    1. Zaczniemy od stworzenia prostego enuma, który będzie reprezentować typ sprawy którą klient chce załatwić w urzędzie.
    Dostępne typy to REGISTER (rejestracja), UNREGISTER(wyrejestrowanie), CHECK_STATUS(stan klienta)
    2. Stwórzmy klasę Client. Każdy klient posiada typ sprawy który musi podać oraz swój pesel, dlatego:
        - stwórz konstruktor w klasie klient, która jako parametr przyjmuje typ sprawy oraz pesel i ustawia wartości tych pól w klasie.
        - stwórz gettery oraz settery tych pól.
    3. Stwórz klasę Office która reprezentuje pojedyncze biuro, w którym będą obsługiwani klienci.
        - w klasie office dodaj metodę handle która obsługuje klienta. Parametrem tej metody będzie klient.
        - w metodzie ma się pojawiać komunikat w formacie: "Klient pesel: {pesel} załatwia sprawę {typ_sprawy}"
    4. Stwórz klasę Department która posiada kolekcję (możesz użyć listy lub mapy) biur.
        - dodaj metodę addOffice która jako parametr przyjmuje biuro.
        - dodaj metodę getOffice która służy do pobierania biura. Jeśli użyłeś/łaś listy to parametrem tej metody powinien być indeks,
         natomiast jeśli użyto mapy, to parametrem powinien być typ klucza tej mapy.
    5. Stwórz maina. W mainie stwórz department, dodaj do niego dwa biura. Zweryfikuj działanie metod dodania i pobrania biura.
     Następnie stwórz obsługiwanie tej funkcjonalności z linii poleceń/konsoli.
     W konsoli powinniśmy mieć możliwość wpisania "dodaj_biuro" które spowoduje dodanie nowego biura.
    -- Jeśli użyto mapy, to zaleca się dopisanie nazwy po "dodaj_biuro" która będzie kluczem, po którym będziemy odnajdować biura.
    Zamysł jest taki, aby - jeśli używamy mapy, to każde biuro ma swoją nazwę i jest po tej nazwie dodawane, a następnie wyszukiwane.
    6. Dodaj w mainie czytanie ze scannera nowego klienta:
    obsluga {PESEL} {TYP_SPRAWY} {ID_POKOJU}
    która tworzy nowego klienta, a następnie wykorzystuje klasę department aby pobrać pokój identyfikowany przez ID_POKOJU i
     obsługuje go metodą handle.
ID_POKOJU == ID_BIURA
https://bitbucket.org/saintamen/itnabanksda/src/0076d23f58e93a8276f865b400c4057990e0fe30/NormalnyUrzad/src/com/amen/somedepartment/?at=master

 */

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Departament department = new Departament();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();

            String trimmedAndLower = line.toLowerCase().trim();
            String[] words = trimmedAndLower.split(" ");
            if (words[0].equals("dodaj_biuro")) {
//                department.addOffice();
                System.out.println("Dodaje biuro!");
                department.addOffice(new Office());
            } else if (words[0].equals("obsluz")) {
                String pesel = words[1];
                String typSprawy = words[2];
                String nrPokoju = words[3];

                try {
                    // Typ sprawy ze string na enum
                    TypSprawy typ = TypSprawy.valueOf(typSprawy);

                    // nr pokoju ze string na liczbe
                    int nr_pokoju = Integer.parseInt(nrPokoju);

                    // Tworze klienta (przekazuje mu sprawe i pesel)
                    Client client = new Client(typ, pesel);

                    // pobieram pokoj
                    Office office = department.getOffice(nr_pokoju);

                    // przekazuje klienta do obsługi do pokoju
                    office.handle(client);
                } catch (NumberFormatException nfe) {
                    System.out.println("Niepoprawny format liczbowy");
                } catch (IllegalArgumentException iae) {
                    System.out.println("Zły argument");
                }
            } else if (words[0].equals("quit")) {
                break;
            }
        }
    }
}




















        /*
        Moje
5. Stwórz maina. W mainie stwórz department, dodaj do niego dwa biura. Zweryfikuj działanie metod dodania i pobrania biura.
     Następnie stwórz obsługiwanie tej funkcjonalności z linii poleceń/konsoli.
     W konsoli powinniśmy mieć możliwość wpisania "dodaj_biuro" które spowoduje dodanie nowego biura.
    -- Jeśli użyto mapy, to zaleca się dopisanie nazwy po "dodaj_biuro" która będzie kluczem, po którym będziemy odnajdować biura.
    Zamysł jest taki, aby - jeśli używamy mapy, to każde biuro ma swoją nazwę i jest po tej nazwie dodawane, a następnie wyszukiwane.

        Scanner sc = new Scanner(System.in);

        departament.addOffice();
        departament.addOffice();
        do {
            System.out.println("");
            String addClientType= sc.nextLine();
            int addPesel = sc.nextInt();

         try{   new Client(addClientType,addPesel);
        }catch (InputMismatchException ime){
             System.out.println("Bad Client Format");
             break;
         } while (sc.hasNextLine());


        do {
            String command = sc.nextLine();
            if (command.equals("quit")){return;}

          else if (command.equals("dodaj_biuro")){
                departament.addOffice();
            }
           else if (command.equals("znajdź biuro")){
                System.out.println("Podaj numer biura");
              int  i = sc.nextInt();
                System.out.println();
                System.out.println("Biuro nr: "+departament.getOffice(i));
            }
        }while (sc.hasNextLine());

    }
}
 */