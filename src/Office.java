/*
3. Stwórz klasę Office która reprezentuje pojedyncze biuro, w którym będą obsługiwani klienci.
        - w klasie office dodaj metodę handle która obsługuje klienta. Parametrem tej metody będzie klient.
        - w metodzie ma się pojawiać komunikat w formacie: "Klient pesel: {pesel} załatwia sprawę {typ_sprawy}"
 */
/*
 6. Dodaj w mainie czytanie ze scannera nowego klienta:
    obsluga {PESEL} {TYP_SPRAWY} {ID_POKOJU}
    która tworzy nowego klienta, a następnie wykorzystuje klasę department aby pobrać pokój identyfikowany przez ID_POKOJU i
     obsługuje go metodą handle.
 */
public class Office {

    public void handle(Client c){
        System.out.println("Klient pesel: " + c.getPesel() +" załatwia sprawę " + c.getSprawa());
    }


}
