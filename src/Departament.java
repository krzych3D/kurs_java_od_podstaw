import java.util.ArrayList;
import java.util.List;

/*
4. Stwórz klasę Department która posiada kolekcję (możesz użyć listy lub mapy) biur.
        - dodaj metodę addOffice która jako parametr przyjmuje biuro.
        - dodaj metodę getOffice która służy do pobierania biura. Jeśli użyłeś/łaś listy to parametrem tej metody powinien być indeks,
         natomiast jeśli użyto mapy, to parametrem powinien być typ klucza tej mapy.
 */
public class Departament {
    public List<Office> officeList = new ArrayList();
//opcja1
    public void addOffice(Office office) {
        officeList.add(office);
    }
//opcja2
    public void addOffice() {
        officeList.add(new Office());
    }

    public Office getOffice(int i) {

        return officeList.get(i);
    }
}
