import com.sun.org.apache.regexp.internal.RE;

import java.time.LocalDateTime;
import java.util.*;

public class Database {
    private Map<Integer, Record> map = new HashMap<>();
    private static int counter = 0;

    public void addRecord(Record rc) {
        map.put(counter++, rc);
    }

    public void refresh() {

//        Map.Entry<Integer, Record>
        Iterator<Map.Entry<Integer, Record>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            // każde wywołanie iterator.next spowoduje przejscie
            // do nastepnego elementu.
            Map.Entry<Integer, Record> wpisWMapie = iterator.next();

            Record record = wpisWMapie.getValue();
            // sprawdzam czy czas przedawnienia jest po obecnej dacie i godzinie
//            if (record.getLocalDateTimePrzedawnione().isAfter(LocalDateTime.now())) {
            if (record.getCzasPrzedawnienia().isAfter(LocalDateTime.now())) {
                // z mapy usuwamy podając w parametrze
                // metody remove klucz obiektu
                map.remove(wpisWMapie.getKey());

                //
                System.out.println("Usuwam wpis o id: " + wpisWMapie.getKey());
            }

            // iterator.remove spowoduje usuniecie ostatnio wyciagnietego
//            iterator.remove();
        }

    }

    public void iterujKolekcje() {
        Set<JakasKlasa> zbior = new HashSet();

        Iterator<JakasKlasa> iterator = zbior.iterator();
        while (iterator.hasNext()) {
            JakasKlasa elementZbioru = iterator.next();

            // działania
            if (elementZbioru.isField()) {
                System.out.println("Field jest ustawiony");
            }
        }
    }


    public void iterujMape() {
        Map<Integer, JakasKlasa> mapa = new HashMap();


        Iterator<Integer> iterator = mapa.keySet().iterator();
        while (iterator.hasNext()) {
            Integer indeksMapy = iterator.next();

            JakasKlasa wartoscSpodKlucza = mapa.get(indeksMapy);
            // działania
            if (true) {
                System.out.println("Field jest ustawiony");
            }
        }
    }

    public void print() {
        System.out.println("Drukuuu");
    }
}