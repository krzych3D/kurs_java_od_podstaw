/*
Zadanie 36:
Stwórz aplikację, która dodaje wpisy do rejestru, a po przedawnieniu usuwa je w obiegu ktory odswieza baze danych.
1. Stworz klase Record ktora reprezentuje pojedynczy wpis w bazie danych. klasa powinna posiadac:
    -  czas dodania - data lub timestamp
    -  czas waznosci - datę przedawnienia lub czas w milisekundach po jakim rekord ulega przedawnieniu
    - identyfikator sprawy - liczba
    - nazwę sprawy - tekst, moze byc jedno slowo
2. Stworz klase Database ktora posiada mapę rekordów Record.
    - dodaj metodę dodawania rekordów do mapy. Rekordy powinny byc ladowane z konsoli
    - dodaj metodę szukania rekordów po ich identyfikatorze
    - dodaj metodę refresh() która wykonuje obieg przez wszystkie rekordy bazy danych i sprawdza czy rekord nie jest przedawniony.
    Jeśli rekord jest przedawniony powinien zostać usunięty a w konsoli powinien się pojawić odpowiedni komunikat.
Dodaj maina, w którym możesz dodawać lub odswieżać rekordy w bazie

https://bitbucket.org/saintamen/itnabanksda/src/0076d23f58e93a8276f865b400c4057990e0fe30/PrzedawnioneRekordy/src/com/amen/records/?at=master
 */



import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Database database = new Database();
        // utworzenie bazy

        while (sc.hasNextLine()) {
            String line = sc.nextLine();

            if(line.equals("quit")){
                break;
            }else if(line.equals("refresh")){
                database.refresh();
                continue;
            }else if(line.equals("print")){
                database.print();
                continue;
            }
            String[] splits = line.split(" ");
            parseSplits(splits, database);
        }
    }

    private static void parseSplits(String[] splits, Database database) {
        // weryfikacja ilości parametrów
        if (splits.length < 4) {
            System.err.println("Niewłaściwa ilość parametrów.");
            return;
        }


        //pierwszy element parsowania / analizy to konwersja string na datę w odpowiednim formacie
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH:mm");
        LocalDateTime ldt = null;
        try {
            ldt = LocalDateTime.parse(splits[0], formatter);
        } catch (DateTimeParseException e) {
            System.err.println("Zły format daty - proszę o podanie daty w formacie: yyyy-MM-dd-HH:mm");
            return;
        }
        long czas_waznosci = 0;
        try {
            czas_waznosci = Long.parseLong(splits[1]);
        } catch (NumberFormatException nfe) {
            System.err.println("Niepoprawna liczba podana jako parametr 2: " + nfe.getMessage());
            return;
        }

        int id = 0;
        try {
            id = Integer.parseInt(splits[2]);
        } catch (NumberFormatException nfe) {
            System.err.println("Niepoprawna liczba podana jako parametr 3: " + nfe.getMessage());
            return;
        }

        // bad..
        // zły string.. niedobry string
        // suboptymalny - przepisuje bufor
//		String str = "";
//		for(int i =3 ; i< splits.length ; i++){
//			str += splits[i] + " ";
//		}
//		str = str.substring(0, str.length()-1);
//		System.out.println("Linia:" + str);

        StringBuilder str = new StringBuilder();
        for(int i =3 ; i< splits.length ; i++){
            str.append(splits[i]).append(" ");
        }
        String finalString = str.substring(0, str.length()-1);
        System.out.println("Linia:" + finalString);

        Record r = new Record(ldt, czas_waznosci, id, finalString);
        database.addRecord(r);
    }
}